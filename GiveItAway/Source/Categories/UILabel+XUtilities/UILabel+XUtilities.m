//
//  UILabel+XUtilities.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "UILabel+XUtilities.h"

//################################################################################
@implementation UILabel (XUtilities)

//################################################################################
- (void)addAwesomeIcon:(FAIcon)icon beforeTitle:(BOOL)before
{
    NSString *iconString = [NSString stringFromFontAwesomeIcon:icon];
    self.font = [UIFont fontWithName:kFontAwesomeFont size:self.font.pointSize];
    
    NSString *title = [NSString stringWithFormat:@"%@", iconString];
    
    if(self.text && ![self.text isEmpty])
    {
        if(before)
            title = [title stringByAppendingFormat:@" %@", self.text];
        else
            title = [NSString stringWithFormat:@"%@  %@", self.text, iconString];
    }
    
    self.text = title;
}

//################################################################################
@end
//################################################################################
