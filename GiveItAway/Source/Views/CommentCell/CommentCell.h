//
//  CommentCell.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <UIKit/UIKit.h>
#import "GIAComment.h"

//################################################################################
@interface CommentCell : UITableViewCell

@property (nonatomic, strong) GIAComment * comment;

- (void) updateUI;
+ (CGFloat) cellHeight;
+ (NSString *) cellIdentifier;

//################################################################################
@end
//################################################################################
