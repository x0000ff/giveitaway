//
//  CommentCell.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "CommentCell.h"

//################################################################################
#import "GIAComment.h"
#import "GIAUser.h"
#import "UIImageView+AFNetworking.h"

//################################################################################
@interface CommentCell ()

@property (nonatomic, strong) IBOutlet UIImageView * userAvatarIV;

@property (nonatomic, strong) IBOutlet UILabel * userNameLbl;
@property (nonatomic, strong) IBOutlet UILabel * contentLbl;

@property (nonatomic, strong) IBOutlet UILabel * dateLbl;

@end

//################################################################################
@implementation CommentCell

//################################################################################
- (void) setComment:(GIAComment *)comment
{
    _comment = comment;
    [self updateUI];
}

//################################################################################
- (void) updateUI
{
    self.contentLbl.text = self.comment.content;
    self.dateLbl.text = [NSString formatLocaleDate:self.comment.createdAt];
    self.userNameLbl.text = self.comment.user.username;
    
    NSURL * avatarUrl = self.comment.user.avatarNormalUrl;
    [self.userAvatarIV setImageWithURL:avatarUrl placeholderImage:[UIImage imageNamed:@"fb_blank_profile_portrait.png"]];
}

//################################################################################
+ (CGFloat) cellHeight
{
    return 62;
}

//################################################################################
+ (NSString *) cellIdentifier
{
    return @"CommentCell";
}

//################################################################################
@end
//################################################################################
