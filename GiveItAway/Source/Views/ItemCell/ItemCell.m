//
//  ItemCell.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "ItemCell.h"

//################################################################################
#import "GIAItem.h"
#import "GIAItemPhoto.h"
#import "GIAUser.h"
#import "GIACategory.h"
#import "UIImageView+AFNetworking.h"

//################################################################################
@interface ItemCell ()

@property (nonatomic, strong) IBOutlet UIImageView * itemIV;
@property (nonatomic, strong) IBOutlet UIImageView * userAvatarIV;

@property (nonatomic, strong) IBOutlet UILabel * titleLbl;
@property (nonatomic, strong) IBOutlet UILabel * descriptionLbl;
@property (nonatomic, strong) IBOutlet UILabel * categoryLbl;

@property (nonatomic, strong) IBOutlet UILabel * dateLbl;

@end

//################################################################################
@implementation ItemCell

//################################################################################
- (void) setItem:(GIAItem *)item
{
    _item = item;
    [self updateUI];
}

//################################################################################
- (void) updateUI
{
    self.titleLbl.text = self.item.title;
    self.descriptionLbl.text = [self.item.desc flattenHTML];
    self.categoryLbl.text = self.item.category.name;
    self.dateLbl.text = [NSString formatLocaleDate:self.item.created_at];
    
    NSURL * imageUrl = [[self.item.photos firstObject] imageMediumUrl];
    [self.itemIV  setImageWithURL:imageUrl];

    NSURL * avatarUrl = self.item.user.avatarNormalUrl;
    [self.userAvatarIV setImageWithURL:avatarUrl placeholderImage:[UIImage imageNamed:@"fb_blank_profile_portrait.png"]];
}

//################################################################################
+ (CGFloat) cellHeight
{
    return 120;
}

//################################################################################
+ (NSString *) cellIdentifier
{
    return @"ItemCell";
}

//################################################################################
@end
//################################################################################
