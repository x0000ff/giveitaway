//
//  ItemCell.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <UIKit/UIKit.h>
#import "GIAItem.h"

//################################################################################
@interface ItemCell : UITableViewCell

@property (nonatomic, strong) GIAItem * item;

- (void) updateUI;
+ (CGFloat) cellHeight;
+ (NSString *) cellIdentifier;

//################################################################################
@end
//################################################################################
