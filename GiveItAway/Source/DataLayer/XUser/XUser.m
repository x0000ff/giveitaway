//
//  XUser.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "XUser.h"

//################################################################################
@interface XUser ()

//@property (nonatomic, strong) GIAUser * user;

@property (nonatomic, strong) NSString * token;

@property (nonatomic, assign) BOOL isLogging;

@end

//################################################################################
@implementation XUser

//################################################################################
static NSString * const kXUserKey = @"kXUserKey";

static NSString * const kXUserTokenKey = @"kXUserTokenKey";
static NSString * const kGIAUserKey = @"kGIAUserKey";

//################################################################################
+ (XUser *) current
{
    static XUser * current = nil;
    static dispatch_once_t once_t;
    
    dispatch_once(&once_t, ^
                  {
                      current = [[XUser alloc] init];
                      current.isLogging = NO;
                      
                      NSLog(@">>> %@", [self.class currentState]);
                      [current load];
//                      [self.class loadCachedSession];
                  });
    
    return current;
}

//################################################################################
#pragma mark - Facebook stuff

//################################################################################
+ (void) postFacebookStateDidChange
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kFacebookStateDidChange object:nil];
}

//################################################################################
+ (BOOL) isLogged
{
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) return YES;
    if (FBSession.activeSession.state == FBSessionStateOpen) return YES;
    
    return NO;
}

//################################################################################
+ (NSArray *)fbLoginPermissions
{
    NSArray * permissions = @[
                              @"email",
                              @"user_about_me",
                              @"user_birthday",
                              @"user_hometown",
                              @"user_interests",
                              @"user_status",
                              @"user_website",
                              @"read_requests",
                              @"read_stream",
                              @"user_photos",
                              @"user_location",
                              @"friends_about_me",
                              ];
    
    return permissions;
}

//################################################################################
+ (void)login
{
    [self loginForce:NO];
}

//################################################################################
+ (void)loginForce:(BOOL)force
{
//    NSLog(@"xxx");
    if (!force)
    {
        if ([XUser isLogged]) return;
    }
    if ([XUser current].isLogging) return;

//    NSLog(@"zzz");
    
    [XUser current].isLogging = YES;
    [self postFacebookStateDidChange];
    
    [FBSession openActiveSessionWithReadPermissions:[self fbLoginPermissions]
                                       allowLoginUI:YES
                                  completionHandler: ^(FBSession *session, FBSessionState state, NSError *error) {
         [[self current] sessionStateChanged:session state:state error:error];
     }];
}

//################################################################################
+ (void) logout
{
    [FBSession.activeSession closeAndClearTokenInformation];
    [[self current] clearAllData];
    
    [self postFacebookStateDidChange];
}

//################################################################################
- (void) clearAllData
{
    self.token = nil;
    self.user = nil;

    [self save];
}

//################################################################################
+ (void) loadCachedSession
{
    if (!FBSession.activeSession.isOpen) {
        if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded)
        {
            // even though we had a cached token, we need to login to make the session usable
            [self loginForce:YES];
        }
    }
}

//################################################################################
+ (BOOL)handleOpenURL:(NSURL *)url
{
    return [FBSession.activeSession handleOpenURL:url];
}

//################################################################################
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    BOOL sendUpdate = YES;
    switch (state) {
        case FBSessionStateOpen:
        {
            NSString * fbToken = [[[FBSession activeSession] accessTokenData] accessToken];
            
            [[GIAClient sharedClient] loginMeWithFacebookToken:fbToken block:^(NSString *token, GIAUser * user, NSError *error) {
                self.token = token;
                self.user = user;
                [self save];
                [self.class postFacebookStateDidChange];
            }];
            return;
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            sendUpdate = NO;
            [self.class logout];
            break;
        default:
            break;
    }
    
    NSLog(@"1 >>>> %@", [self.class currentState]);
    [XUser current].isLogging = NO;
    
    if (sendUpdate)
    {
        [self.class postFacebookStateDidChange];
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

//################################################################################
+ (NSString *) currentState
{
    NSString * stateString = @"Unknown";
    if (!FBSession.activeSession) return stateString;
    
    FBSessionState state = FBSession.activeSession.state;
    
    switch (state) {
        case FBSessionStateCreated:
            stateString = @"Created";
            break;
        case FBSessionStateCreatedTokenLoaded:
            stateString = @"Token Loaded";
            break;
        case FBSessionStateCreatedOpening:
            stateString = @"Opening";
            break;
        case FBSessionStateOpen:
            stateString = @"Open";
            break;
        case FBSessionStateOpenTokenExtended:
            stateString = @"Token Extended";
            break;
        case FBSessionStateClosedLoginFailed:
            stateString = @"Failed";
            break;
        case FBSessionStateClosed:
            stateString = @"Closed";
            break;
    }
    
    return stateString;
}

//################################################################################
+ (UIImage *) avatarPlaceholderImage
{
    return [UIImage imageNamed:@"fb_blank_profile_portrait.png"];
}

//################################################################################
- (NSURL *) avatarUrlForWidth:(NSUInteger)width height:(NSUInteger)height
{
//    NSString * fbId = self.user.fb_id;
    NSString * fbId = self.user.uid;
    if (!fbId) return nil;
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=%d&height=%d", fbId, width, height]];
}

//################################################################################
- (NSURL *) avatarUrl
{
    return [self avatarUrlForWidth:240 height:240];
}

//################################################################################
- (NSURL *) avatarThumbUrl
{
    return [self avatarUrlForWidth:120 height:120];
}

//################################################################################
- (void) load
{
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    self.token = [def objectForKey:kXUserTokenKey];
}

//################################################################################
- (void) save
{
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    if (self.token) [def setObject:self.token forKey:kXUserTokenKey];
    [def synchronize];
}

//################################################################################
@end
//################################################################################
