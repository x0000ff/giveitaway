//
//  XUser.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "GIAUser.h"

//################################################################################
static NSString * const kFacebookStateDidChange = @"kFacebookStateDidChange";

//################################################################################
@interface XUser : NSObject

//################################################################################
//@property (nonatomic, strong, readonly) GIAUser * user;
@property (nonatomic, strong) GIAUser * user;

@property (nonatomic, strong, readonly) NSString * token;

@property (nonatomic, assign, readonly) BOOL isLogging;

//################################################################################
+ (XUser *) current;

+ (BOOL) handleOpenURL:(NSURL *)url;

+ (void) login;
+ (void) logout;
+ (BOOL) isLogged;

+ (UIImage *) avatarPlaceholderImage;
- (NSURL *) avatarUrl;
- (NSURL *) avatarThumbUrl;

- (void) save;
+ (void) postFacebookStateDidChange;
+ (void) loadCachedSession;

//################################################################################
@end
//################################################################################
