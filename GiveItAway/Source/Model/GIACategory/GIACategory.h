//
//  GIACategory.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>

//################################################################################
@interface GIACategory : NSObject

@property (nonatomic, strong) NSNumber * id;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * slug;

@property (nonatomic, strong) NSDate * created_at;
@property (nonatomic, strong) NSDate * updated_at;

@property (nonatomic, strong) NSNumber * gifts_count;

//################################################################################
- (instancetype)initWithAttributes:(NSDictionary *)attributes;

//################################################################################
@end
//################################################################################
