//
//  GIACategory.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "GIACategory.h"

//################################################################################
@implementation GIACategory

//################################################################################
- (instancetype)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self)
    {
        return nil;
    }
    
    self.id = [attributes valueForKeyPath:@"id"];
    self.name = [attributes valueForKeyPath:@"name"];
    self.slug = [attributes valueForKeyPath:@"slug"];
    
    self.created_at = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"created_at"]];
    self.updated_at = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"updated_at"]];
    
    self.gifts_count = [attributes valueForKeyPath:@"gifts_count"];

    return self;
}

//################################################################################
@end
//################################################################################
