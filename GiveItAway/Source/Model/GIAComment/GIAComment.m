//
//  GIAComment.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "GIAComment.h"
#import "GIAUser.h"

//################################################################################
@implementation GIAComment

//################################################################################
- (instancetype)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self)
    {
        return nil;
    }
    
    [self fullfillWithAttributs:attributes];
    return self;
}

//################################################################################
- (void) fullfillWithAttributs:(NSDictionary *)attributes
{
    self.id = [attributes valueForKeyPath:@"id"];
    self.createdAt = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"created_at"]];
    self.updatedAt = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"updated_at"]];
    self.userId = [attributes valueForKeyPath:@"user_id"];
    self.typeId = [attributes valueForKeyPath:@"type_id"];
    self.giftId = [attributes valueForKeyPath:@"gift_id"];
    self.content = [attributes valueForKeyPath:@"content"];

    self.user = [[GIAUser alloc] initWithAttributes:[attributes valueForKeyPath:@"user"]];
}

//################################################################################
- (BOOL) isMine
{
    if (![XUser current].user.id) return NO;
    
    return ([[XUser current].user.id isEqual:self.userId]);
}

//################################################################################
@end
//################################################################################
