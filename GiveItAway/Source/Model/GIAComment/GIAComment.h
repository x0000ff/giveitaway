//
//  GIAComment.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"

@class GIAUser;

//################################################################################
@interface GIAComment : NSObject

@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSDate * createdAt;
@property (nonatomic, strong) NSDate * updatedAt;
@property (nonatomic, strong) NSNumber * id;
@property (nonatomic, strong) NSNumber * giftId;
@property (nonatomic, strong) NSNumber * userId;
@property (nonatomic, strong) NSNumber * typeId;
@property (nonatomic, strong) GIAUser * user;

@property (nonatomic, assign, readonly) BOOL isMine;

//################################################################################
- (instancetype)initWithAttributes:(NSDictionary *)attributes;
- (void) fullfillWithAttributs:(NSDictionary *)attributes;

//################################################################################
@end
//################################################################################
