//
//  GIAItemPhoto.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"

//################################################################################
@interface GIAItemPhoto : NSObject

@property (nonatomic, strong) NSDate * created_at;
@property (nonatomic, strong) NSDate * updated_at;
@property (nonatomic, strong) NSNumber * gift_id;
@property (nonatomic, strong) NSNumber * id;

@property (nonatomic, strong) NSURL * imageUrl;
@property (nonatomic, strong) NSURL * imageMiniUrl;
@property (nonatomic, strong) NSURL * imageMediumUrl;
@property (nonatomic, strong) NSURL * imageNormaUrl;

//################################################################################
- (instancetype)initWithAttributes:(NSDictionary *)attributes;

//################################################################################
@end
//################################################################################
