//
//  GIAItemPhoto.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "GIAItemPhoto.h"

//################################################################################
@implementation GIAItemPhoto

//################################################################################
- (instancetype)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self)
    {
        return nil;
    }
    
    self.id = [attributes valueForKeyPath:@"id"];
    
    self.created_at = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"created_at"]];
    self.updated_at = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"updated_at"]];
    
    self.gift_id = [attributes valueForKeyPath:@"gift_id"];

    NSDictionary * image = [attributes valueForKeyPath:@"image"];
    self.imageUrl = [NSURL URLWithString:[image valueForKeyPath:@"url"]];

    self.imageMiniUrl = [NSURL URLWithString:[[image valueForKeyPath:@"mini"] valueForKeyPath:@"url"]];
    self.imageMediumUrl = [NSURL URLWithString:[[image valueForKeyPath:@"medium"] valueForKeyPath:@"url"]];
    self.imageNormaUrl = [NSURL URLWithString:[[image valueForKeyPath:@"normal"] valueForKeyPath:@"url"]];
    
    return self;
}

//################################################################################
@end
//################################################################################
