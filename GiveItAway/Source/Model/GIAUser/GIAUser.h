//
//  GIAUser.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"

//################################################################################
@interface GIAUser : NSObject

@property (nonatomic, strong) NSURL * avatarUrl;
@property (nonatomic, strong) NSURL * avatarMiniUrl;
@property (nonatomic, strong) NSURL * avatarMediumUrl;
@property (nonatomic, strong) NSURL * avatarNormalUrl;

@property (nonatomic, strong) NSDate * created_at;
@property (nonatomic, strong) NSDate * updated_at;

@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * first_name;
@property (nonatomic, strong) NSNumber * id;

@property (nonatomic, strong) NSString * language;
@property (nonatomic, strong) NSString * last_name;
@property (nonatomic, strong) NSString * profile;

@property (nonatomic, strong) NSString * provider;
@property (nonatomic, strong) NSString * slug;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * username;


//################################################################################
- (instancetype) initWithAttributes:(NSDictionary *)attributes;
+ (void) myProfileWithBlock:(void (^)(GIAUser * user, NSError *error))block;

//################################################################################
@end
//################################################################################
