//
//  GIAUser.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "GIAUser.h"

//################################################################################
@implementation GIAUser

//################################################################################
- (instancetype)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self)
    {
        return nil;
    }
    
    self.id = [attributes valueForKeyPath:@"id"];

    self.created_at = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"created_at"]];
    self.updated_at = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"updated_at"]];

    self.email = [attributes valueForKeyPath:@"email"];
    self.first_name = [attributes valueForKeyPath:@"first_name"];
    self.last_name = [attributes valueForKeyPath:@"last_name"];
    self.language = [attributes valueForKeyPath:@"language"];
    self.profile = [attributes valueForKeyPath:@"profile"];
    self.provider = [attributes valueForKeyPath:@"provider"];
    self.slug = [attributes valueForKeyPath:@"slug"];
    self.uid = [attributes valueForKeyPath:@"uid"];
    self.username = [attributes valueForKeyPath:@"username"];

    NSArray * avatars = [attributes valueForKeyPath:@"avatar"];
    self.avatarUrl = [NSURL URLWithString:[avatars valueForKeyPath:@"url"]];
    self.avatarMiniUrl = [NSURL URLWithString:[[avatars valueForKeyPath:@"mini"] valueForKeyPath:@"url"]];
    self.avatarMediumUrl = [NSURL URLWithString:[[avatars valueForKeyPath:@"medium"] valueForKeyPath:@"url"]];
    self.avatarNormalUrl = [NSURL URLWithString:[[avatars valueForKeyPath:@"normal"] valueForKeyPath:@"url"]];

    return self;
}

//################################################################################
+ (void) myProfileWithBlock:(void (^)(GIAUser * user, NSError *error))block
{
    NSDictionary * params = nil;
    NSString * authToken = [XUser current].token;
    if (!authToken)
    {
        return;
    }

    params = @{kTokenKey: authToken};

    [[GIAClient sharedClient] GET:kMyProfileUrlPath parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSDictionary * attributes = (NSDictionary *)json;
        GIAUser * user = [[GIAUser alloc] initWithAttributes:attributes];

        if (block) {
            block(user, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

//################################################################################
@end
//################################################################################
