//
//  GIAItem.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"

@class GIAUser;
@class GIACategory;

//################################################################################
@interface GIAItem : NSObject

@property (nonatomic, strong) NSNumber * id;
@property (nonatomic, strong) NSNumber * user_id;
@property (nonatomic, strong) NSNumber * category_id;

@property (nonatomic, strong) NSDate * created_at;
@property (nonatomic, strong) NSDate * updated_at;

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) NSString * state;

@property (nonatomic, strong) NSString * permalink;
@property (nonatomic, strong) GIAUser * user;
@property (nonatomic, strong) GIACategory * category;
@property (nonatomic, strong) NSArray * photos;

@property (nonatomic, strong) NSNumber * canBeWishedByMe;
@property (nonatomic, strong) NSNumber * wishersCount;
@property (nonatomic, strong) NSNumber * commentsCount;

@property (nonatomic, assign, readonly) BOOL isMine;

//################################################################################
- (instancetype)initWithAttributes:(NSDictionary *)attributes;
- (void) fullfillWithAttributs:(NSDictionary *)attributes;

//################################################################################
@end
//################################################################################
