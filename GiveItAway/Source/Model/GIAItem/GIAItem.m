//
//  GIAItem.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "GIAItem.h"
#import "GIAUser.h"
#import "GIACategory.h"
#import "GIAItemPhoto.h"

//################################################################################
@implementation GIAItem

//################################################################################
- (instancetype)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self)
    {
        return nil;
    }
    
    [self fullfillWithAttributs:attributes];
    return self;
}

//################################################################################
- (void) fullfillWithAttributs:(NSDictionary *)attributes
{
    
    self.id = [attributes valueForKeyPath:@"id"];
    self.category_id = [attributes valueForKeyPath:@"category_id"];
    self.user_id = [attributes valueForKeyPath:@"user_id"];
    
    self.created_at = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"created_at"]];
    self.updated_at = [GIAClient parseDateFromString:[attributes valueForKeyPath:@"updated_at"]];
    
    self.title = [attributes valueForKeyPath:@"title"];
    self.desc = [attributes valueForKeyPath:@"description"];
    self.state = [attributes valueForKeyPath:@"state"];
    self.permalink = [attributes valueForKeyPath:@"permalink"];
    self.canBeWishedByMe = [attributes valueForKeyPath:@"can_be_wished_by?"];
    self.wishersCount = [attributes valueForKeyPath:@"wishers_count"];
    self.commentsCount = [attributes valueForKeyPath:@"comments_count"];
    
    
    self.user = [[GIAUser alloc] initWithAttributes:[attributes valueForKeyPath:@"user"]];
    self.category = [[GIACategory alloc] initWithAttributes:[attributes valueForKeyPath:@"category"]];
    
    NSMutableArray * photos = [NSMutableArray array];
    for (NSDictionary * photo in [attributes valueForKeyPath:@"photos"])
    {
        GIAItemPhoto * itemPhoto = [[GIAItemPhoto alloc] initWithAttributes:photo];
        [photos addObject:itemPhoto];
    }
    self.photos = [NSArray arrayWithArray:photos];
}

//################################################################################
- (BOOL) isMine
{
    if (![XUser current].user.id) return NO;
    
    return ([[XUser current].user.id isEqual:self.user.id]);
}

//################################################################################
@end
//################################################################################
