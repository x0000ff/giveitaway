//
//  GIAClient.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "GIAClient.h"
#import "GIAItem.h"
#import "GIAComment.h"

//################################################################################
@interface GIAClient ()

+ (void) loginMeOperationWithFacebookToken:(NSString *)facebookToken block:(void (^)(NSString * token, GIAUser * user, NSError *error))block;

@end

//################################################################################
static NSString * const kAPIBaseURLKey = @"GIAApiBaseURLKey";

//################################################################################
@implementation GIAClient

//################################################################################
+ (instancetype)sharedClient
{
    static GIAClient * _sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[GIAClient alloc] initWithBaseURL:[NSURL URLWithString:[self apiBaseUrl]]];
        [_sharedClient setSecurityPolicy:[AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey]];
    });
    
    return _sharedClient;
}

//################################################################################
+ (NSString *) apiBaseUrl
{
    NSString * apiBaseUrl = [[NSBundle mainBundle] objectForInfoDictionaryKey:kAPIBaseURLKey];
    return apiBaseUrl;
}

//################################################################################
+ (NSDate *)parseDateFromString:(NSString *)dateString;
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.formatterBehavior = NSDateFormatterBehavior10_4;
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];

    return [dateFormatter dateFromString:dateString];
}

//################################################################################
- (void) loginMeWithFacebookToken:(NSString *)facebookToken block:(void (^)(NSString *token, GIAUser * user, NSError *error))block
{
    [GIAClient loginMeOperationWithFacebookToken:facebookToken block:^(NSString *token, GIAUser * user, NSError *error) {
        if (block)
        {
            block(token, user, error);
        }
    }];
}

//################################################################################
+ (void) loginMeOperationWithFacebookToken:(NSString *)facebookToken block:(void (^)(NSString * token, GIAUser * user, NSError *error))block
{
    NSDictionary * params = nil;
    if (facebookToken)
    {
        params = @{kLoginFacebookTokenKey: facebookToken};
    }
    
    [[GIAClient sharedClient] GET:kLoginUrlPath parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSString * tokenFromResponse = [json valueForKeyPath:kLoginResponseTokenKey];
        NSDictionary * userFromResponse = [json valueForKeyPath:kLoginResponseUserTokenKey];
        NSLog(@"LOGIN :: %@", json);
        
        GIAUser * user = [[GIAUser alloc] initWithAttributes:userFromResponse];

        if (block) {
            block(tokenFromResponse, user, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block) {
            block(nil, nil, error);
        }
    }];
}

//################################################################################
- (void) giftWithId:(NSUInteger) giftId block:(void (^)(NSDictionary * giftAttributes, NSError *error))block
{
    NSLog(@"START : %@", NSStringFromSelector(_cmd));
    
    NSString * token = [XUser current].token;
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    if (token)
    {
        [params setValue:token forKey:kTokenKey];
    }
    
    NSString * requestUrl = [NSString stringWithFormat:@"%@%d", kGiftsUrlPath, giftId];
    
    [self GET:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSDictionary * attributes = json;
        NSLog(@"ITEM: \n%@", json);
        
        if (block) {
            block(attributes, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

//################################################################################
- (NSString *) allGiftsWithFilterString:(NSString *)filterString block:(void (^)(NSArray * gifts, NSError *error, NSString * urlString))block
{
    NSString * token = [XUser current].token;
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    if (token)
    {
        [params setValue:token forKey:kTokenKey];
    }
    if (filterString)
    {
        [params setValue:filterString forKey:kFilterKey];
    }
    NSLog(@"\nSTART : %@\nPARAMS: %@", NSStringFromSelector(_cmd), params);
    
    NSString * requestUrl = kGiftsUrlPath;
    
    [self GET:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSArray * objectsFromResponse = json;
        NSLog(@"ITEMS: \n%@", json);
        NSMutableArray * mutableObjects = [NSMutableArray array];
        for (NSDictionary * attributes in objectsFromResponse)
        {
            GIAItem * item = [[GIAItem alloc] initWithAttributes:attributes];
            [mutableObjects addObject:item];
        }

        if (block) {
            block([NSArray arrayWithArray:mutableObjects], nil, requestUrl);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block) {
            block(nil, error, requestUrl);
        }
    }];
    
    return requestUrl;
}

//################################################################################
- (NSString *) myGiftsWithFilterString:(NSString *)filterString block:(void (^)(NSArray * gifts, NSError *error, NSString * urlString))block
{
    NSLog(@"START : %@", NSStringFromSelector(_cmd));
    
    NSString * requestUrl = kGiftsMineUrlPath;

    NSString * token = [XUser current].token;
    if (!token)
    {
        block(nil, nil, requestUrl);
        return requestUrl;
    }
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    if (token)
    {
        [params setValue:token forKey:kTokenKey];
    }
    if (filterString)
    {
        [params setValue:filterString forKey:kFilterKey];
    }
    
    [self GET:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSArray * objectsFromResponse = json;
        NSLog(@"MINE ITEMS: \n%@", json);
        NSMutableArray * mutableObjects = [NSMutableArray array];
        for (NSDictionary * attributes in objectsFromResponse)
        {
            GIAItem * item = [[GIAItem alloc] initWithAttributes:attributes];
            [mutableObjects addObject:item];
        }
        
        if (block) {
            block([NSArray arrayWithArray:mutableObjects], nil, requestUrl);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block) {
            block(nil, error, requestUrl);
        }
    }];
    
    return requestUrl;
}

//################################################################################
- (NSString *) myWishesWithFilterString:(NSString *)filterString block:(void (^)(NSArray * gifts, NSError *error, NSString * urlString))block
{
    NSLog(@"START : %@", NSStringFromSelector(_cmd));
    NSString * token = [XUser current].token;
    NSString * requestUrl = kGiftsIWishUrlPath;
    
    if (!token)
    {
        block(nil, nil, requestUrl);
        return requestUrl;
    }
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    if (token)
    {
        [params setValue:token forKey:kTokenKey];
    }
    if (filterString)
    {
        [params setValue:filterString forKey:kFilterKey];
    }
    
    [self GET:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSArray * objectsFromResponse = json;
        NSLog(@"I WISH ITEMS: \n%@", json);
        NSMutableArray * mutableObjects = [NSMutableArray array];
        for (NSDictionary * attributes in objectsFromResponse)
        {
            GIAItem * item = [[GIAItem alloc] initWithAttributes:attributes];
            [mutableObjects addObject:item];
        }
        
        if (block) {
            block([NSArray arrayWithArray:mutableObjects], nil, requestUrl);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@">>> ERROR: %@\n%@", error.localizedDescription, operation.responseString);
        if (block) {
            block(nil, error, requestUrl);
        }
    }];
    
    return requestUrl;
}

//################################################################################
- (void) wishGift:(GIAItem *)item withBlock:(void (^)(BOOL success, NSError *error))block
{
    [self wishGift:item wish:YES withBlock:block];
}

//################################################################################
- (void) unwishGift:(GIAItem *)item withBlock:(void (^)(BOOL success, NSError *error))block
{
    [self wishGift:item wish:NO withBlock:block];
}

//################################################################################
- (void) wishGift:(GIAItem *)item wish:(BOOL)wish withBlock:(void (^)(BOOL success, NSError *error))block
{
    if (!item)
    {
        if (block)
        {
            block(NO,nil);
        }
    }
    
    NSString * requestUrl = [NSString stringWithFormat:wish ? kWishGiftUrlPath : kUnwishGiftUrlPath, item.id.integerValue];
    NSDictionary * params = @{
                              kTokenKey : [XUser current].token,
                              //                              @"wish":@{kItemIdKey : item.id}
                              };
    NSLog(@"URL: \n%@", requestUrl);
    
    [self GET:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        
        if (wish)
        {
            NSLog(@"WISH GIFT: \n%@", json);
        }
        else
        {
            NSLog(@"UNWISH GIFT: \n%@", json);
        }
        NSNumber * successNumber = [NSNumber numberWithBool:[[json objectForKey:kSuccessKey] boolValue]];
        
        if (block) {
            block(successNumber.boolValue, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@">>> ERROR: %@\n%@", error.localizedDescription, operation.responseString);
        
        if (block) {
            block(NO, error);
        }
    }];
}

//################################################################################
- (void) isGiftWishedByMe:(GIAItem *)item withBlock:(void (^)(BOOL isWishedAlready, NSError *error))block
{
    if (!item)
    {
        if (block)
        {
            block(NO,nil);
        }
    }
    
    NSString * token = [XUser current].token;
    if (!token)
    {
        if (block)
        {
            block(NO,nil);
        }
    }
    
    NSString * requestUrl = [NSString stringWithFormat:kIsGiftWishedByMeUrlPath, item.id.integerValue];
    NSDictionary * params = @{
                              kTokenKey : token,
                              };
    
    NSLog(@"URL: \n%@", requestUrl);
    
    [self GET:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSNumber * wishedNumber = [NSNumber numberWithBool:[[json objectForKey:kWishedKey] boolValue]];
        
        if (block) {
            block(wishedNumber.boolValue, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@">>> ERROR: %@\n%@", error.localizedDescription, operation.responseString);
        
        if (block) {
            block(NO, error);
        }
    }];
}

//################################################################################
- (void) giftComments:(GIAItem *)item withBlock:(void (^)(NSArray * comments, NSError *error))block
{
    if (!item)
    {
        if (block)
        {
            block(nil, nil);
        }
    }
    
    NSString * token = [XUser current].token;
    if (!token)
    {
        if (block)
        {
            block(nil, nil);
        }
    }
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    if (token)
    {
        [params setValue:token forKey:kTokenKey];
    }
    NSString * requestUrl = [NSString stringWithFormat:kGiftCommentsUrlPath, item.id.integerValue];

    [self GET:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSArray * objectsFromResponse = json;
        NSLog(@"GIFT COMMENTS: \n%@", json);
        NSMutableArray * mutableObjects = [NSMutableArray array];
        for (NSDictionary * attributes in objectsFromResponse)
        {
            GIAComment * comment = [[GIAComment alloc] initWithAttributes:attributes];
            [mutableObjects addObject:comment];
        }
        
        if (block) {
            block([NSArray arrayWithArray:mutableObjects], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@">>> ERROR: %@\n%@", error.localizedDescription, operation.responseString);
        if (block) {
            block(nil, error);
        }
    }];
}

//################################################################################
- (void) postCommentForGift:(GIAItem *)item commentText:(NSString *)commentText withBlock:(void (^)(BOOL success, NSString * message, NSError *error))block
{
    if (!item)
    {
        if (block)
        {
            block(false, @"Pass Item here", nil);
        }
    }
    
    NSString * token = [XUser current].token;
    if (!token)
    {
        if (block)
        {
            block(false, @"Unautharized access", nil);
        }
    }

    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    if (token)
    {
        [params setValue:token forKey:kTokenKey];
    }
    if (commentText)
    {
        [params setValue:commentText forKey:kNewCommentContentKey];
    }
   
    NSString * requestUrl = [NSString stringWithFormat:kGiftNewCommentUrlPath, item.id.integerValue];
    
    [self PUT:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"POST COMMENT: \n%@", json);
        
        BOOL success = [[json objectForKey:kSuccessKey] boolValue];
        NSString * msg = [json objectForKey:kMsgKey];
        
        if (block) {
            block(success, msg, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@">>> ERROR: %@\n%@", error.localizedDescription, operation.responseString);
        if (block) {
            block(false, error.localizedDescription, nil);
        }
    }];
}

//################################################################################
@end
//################################################################################
