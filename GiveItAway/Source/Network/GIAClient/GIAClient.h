//
//  GIAClient.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "GIAUser.h"
#import "GIAItem.h"

//################################################################################

// NEW API
static NSString * const kLoginUrlPath = @"api/v1/loginviafb";

static NSString * const kGiftsUrlPath = @"api/v1/gifts/";
static NSString * const kGiftsMineUrlPath = @"api/v1/gifts/mine";
static NSString * const kGiftsIWishUrlPath = @"api/v1/gifts/iwish";

static NSString * const kGiftCommentsUrlPath = @"api/v1/gifts/%d/comments";
static NSString * const kGiftNewCommentUrlPath = @"api/v1/gifts/%d/comments";

static NSString * const kMyProfileUrlPath = @"api/v1/users/me";
static NSString * const kUserProfileUrlPath = @"api/v1/users/user_profile";

static NSString * const kIsGiftWishedByMeUrlPath = @"api/v1/gifts/%d/iswishedbyme";
static NSString * const kWishGiftUrlPath = @"api/v1/gifts/%d/wish";
static NSString * const kUnwishGiftUrlPath = @"api/v1/gifts/%d/unwish";

static NSString * const kLoginFacebookTokenKey = @"token";
static NSString * const kLoginResponseTokenKey = @"auth_token";
static NSString * const kLoginResponseUserTokenKey = @"user";
static NSString * const kTokenKey = @"auth_token";
static NSString * const kItemIdKey = @"gift_id";

static NSString * const kFilterKey = @"q";
static NSString * const kNewCommentContentKey = @"content";

static NSString * const kWishedKey = @"wished";
static NSString * const kMsgKey = @"msg";
static NSString * const kSuccessKey = @"success";

//################################################################################
@interface GIAClient : AFHTTPRequestOperationManager

//################################################################################
+ (instancetype)sharedClient;

- (void) loginMeWithFacebookToken:(NSString *)facebookToken block:(void (^)(NSString *token, GIAUser * user, NSError *error))block;
- (NSString *) allGiftsWithFilterString:(NSString *)filterString block:(void (^)(NSArray * gifts, NSError *error, NSString * urlString))block;
- (NSString *) myGiftsWithFilterString:(NSString *)filterString block:(void (^)(NSArray * gifts, NSError *error, NSString * urlString))block;
- (NSString *) myWishesWithFilterString:(NSString *)filterString block:(void (^)(NSArray * gifts, NSError *error, NSString * urlString))block;

- (void) giftWithId:(NSUInteger) giftId block:(void (^)(NSDictionary * giftAttributes, NSError *error))block;

- (void) wishGift:(GIAItem *)item withBlock:(void (^)(BOOL success, NSError *error))block;
- (void) unwishGift:(GIAItem *)item withBlock:(void (^)(BOOL success, NSError *error))block;
- (void) isGiftWishedByMe:(GIAItem *)item withBlock:(void (^)(BOOL isWishedAlready, NSError *error))block;

- (void) giftComments:(GIAItem *)item withBlock:(void (^)(NSArray * comments, NSError *error))block;
- (void) postCommentForGift:(GIAItem *)item commentText:(NSString *)commentText withBlock:(void (^)(BOOL success, NSString * message, NSError *error))block;

+ (NSDate *)parseDateFromString:(NSString *)dateString;

//################################################################################
+ (NSString *) apiBaseUrl;

//################################################################################
@end
//################################################################################
