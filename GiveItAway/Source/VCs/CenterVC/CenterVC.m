//
//  CenterVC.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "CenterVC.h"
#import "IIViewDeckController.h"

//################################################################################
@interface CenterVC ()<IIViewDeckControllerDelegate>

- (IIViewDeckController *)topViewDeckController;

@end

//################################################################################
@implementation CenterVC

//################################################################################
- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// using Font-Awesome on UIBarButtonItem
//    NSString * title = [NSString stringFromFontAwesomeIcon:FAIconReorder];
    NSString * title = [NSString stringWithFormat:@" %@ ", [NSString stringFromFontAwesomeIcon:FAIconReorder]];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:self.viewDeckController
                                                                            action:@selector(toggleLeftView)];
    UIFont * awesomeFont = [UIFont fontWithName:kFontAwesomeFont size:20];

    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{UITextAttributeFont:awesomeFont}
                                                         forState:UIControlStateNormal];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithWhite:0.2 alpha:1];
}

//################################################################################
- (IIViewDeckController *) topViewDeckController
{
    return self.viewDeckController;
}

//################################################################################
@end
//################################################################################
