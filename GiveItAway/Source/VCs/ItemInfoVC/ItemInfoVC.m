//
//  ItemInfoVC.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 06.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "ItemInfoVC.h"

#import "GIAItem.h"
#import "GIAItemPhoto.h"
#import "GIAUser.h"
#import "GIACategory.h"

#import "UIImageView+AFNetworking.h"

#import "PhotoVC.h"
#import "UserProfileVC.h"
#import "GiftCommentsVC.h"

//################################################################################
@interface ItemInfoVC () <MBProgressHUDDelegate>

@property (nonatomic, strong) IBOutlet UIImageView * itemIV;
@property (nonatomic, strong) IBOutlet UIImageView * userAvatarIV;

@property (nonatomic, strong) IBOutlet UILabel * titleLbl;
@property (nonatomic, strong) IBOutlet UITextView * descriptionTV;
@property (nonatomic, strong) IBOutlet UILabel * categoryLbl;

@property (nonatomic, strong) IBOutlet UILabel * dateLbl;
@property (nonatomic, strong) IBOutlet UILabel * userNameLbl;
@property (nonatomic, strong) IBOutlet BButton * wishUnwishBtn;

@property (nonatomic, strong) IBOutlet UILabel * wishersCountLbl;
@property (nonatomic, strong) IBOutlet UILabel * commentsCountLbl;

@property (nonatomic, strong) MBProgressHUD * refreshHUD;
@property (nonatomic, assign) BOOL doNotUpdateWishState;

- (IBAction)doWishUnwish:(id)sender;
- (IBAction)doRefresh:(id)sender;

@end

//################################################################################
@implementation ItemInfoVC

//################################################################################
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.wishersCountLbl.font = [UIFont iconicFontOfSize:20];
    self.wishersCountLbl.text = nil;
    
    self.commentsCountLbl.font = [UIFont iconicFontOfSize:20];
    self.commentsCountLbl.text = nil;
}

//################################################################################
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUI];
    
    if (!self.doNotUpdateWishState)
    {
        self.doNotUpdateWishState = NO;
        [self updateWishedByMeStatus];
    }
}

//################################################################################
- (void) updateUI
{
    if (!self.isViewLoaded) return;
    
    self.titleLbl.text = self.item.title;
    self.descriptionTV.text = [self.item.desc flattenHTML];
    self.categoryLbl.text = self.item.category.name;
    self.userNameLbl.text = self.item.user.username;
    self.dateLbl.text = [NSString formatLocaleDate:self.item.created_at];
    
    NSURL * imageUrl = [[self.item.photos firstObject] imageMediumUrl];
    [self.itemIV  setImageWithURL:imageUrl];
    
    NSURL * avatarUrl = self.item.user.avatarMediumUrl;
    [self.userAvatarIV  setImageWithURL:avatarUrl];

    self.wishersCountLbl.text = [NSString stringWithFormat:@"%@ %d", [NSString stringFromFontAwesomeIcon:FAIconStar], self.item.wishersCount.intValue];
    self.commentsCountLbl.text = [NSString stringWithFormat:@"%@ %d", [NSString stringFromFontAwesomeIcon:FAIconComment], self.item.commentsCount.intValue];

    
    self.wishUnwishBtn.titleLabel.text = nil;
    if (self.item.canBeWishedByMe.boolValue)
    {
        NSString * title = @"Lo quiero!";
        [self.wishUnwishBtn setType:BButtonTypeSuccess];
        self.wishUnwishBtn.titleLabel.text = title;
        [self.wishUnwishBtn setTitle:title forState:UIControlStateNormal];
        [self.wishUnwishBtn addAwesomeIcon:FAIconThumbsUp beforeTitle:YES];
    }
    else
    {
        NSString * title = @"No quiero";
        [self.wishUnwishBtn setType:BButtonTypeWarning];
        self.wishUnwishBtn.titleLabel.text = title;
        [self.wishUnwishBtn setTitle:title forState:UIControlStateNormal];
        [self.wishUnwishBtn addAwesomeIcon:FAIconThumbsDown beforeTitle:YES];
    }
    
    self.wishUnwishBtn.hidden = self.item.isMine;
}

//################################################################################
- (void) updateWishedByMeStatus
{
    if (![XUser current].token)
    {
        self.item.canBeWishedByMe = @YES;
        return;
    }
    
    [self showHUD:@"Update..."];
    [[GIAClient sharedClient] isGiftWishedByMe:self.item withBlock:^(BOOL isWishedAlready, NSError *error) {
        [self hideHUD];
        
        self.item.canBeWishedByMe = [NSNumber numberWithBool:!isWishedAlready];
        [self updateUI];
    }];
}

//################################################################################
- (IBAction)doRefresh:(id)sender
{
    [self showHUD:@"Update..."];
    [[GIAClient sharedClient] giftWithId:self.item.id.intValue block:^(NSDictionary *giftAttributes, NSError *error) {
        [self hideHUD];
        
        [self.item fullfillWithAttributs:giftAttributes];
        [self updateUI];
    }];
}

//################################################################################
- (void) setItem:(GIAItem *)item
{
    _item = item;
    [self updateUI];
}

//################################################################################
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPhoto"])
    {
        self.doNotUpdateWishState = YES;
       PhotoVC * vc = nil;
        
        if ([segue.destinationViewController isKindOfClass:UINavigationController.class])
        {
            vc = (PhotoVC *) [segue.destinationViewController viewControllers][0];
        }
        else
        {
            vc = (PhotoVC *) segue.destinationViewController;
        }
        
        vc.photo = [self.item.photos lastObject];
    }
    else if ([segue.identifier isEqualToString:@"showUserProfile"])
    {
        self.doNotUpdateWishState = YES;
        UserProfileVC * vc = nil;
        
        if ([segue.destinationViewController isKindOfClass:UINavigationController.class])
        {
            vc = (UserProfileVC *) [segue.destinationViewController viewControllers][0];
        }
        else
        {
            vc = (UserProfileVC *) segue.destinationViewController;
        }
        
        vc.user = self.item.user;
    }
    else if ([segue.identifier isEqualToString:@"showGiftComments"])
    {
        self.doNotUpdateWishState = YES;
        GiftCommentsVC * vc = nil;
        
        if ([segue.destinationViewController isKindOfClass:UINavigationController.class])
        {
            vc = (GiftCommentsVC *) [segue.destinationViewController viewControllers][0];
        }
        else
        {
            vc = (GiftCommentsVC *) segue.destinationViewController;
        }
        
        vc.item = self.item;
    }
}

//################################################################################
- (IBAction)doWishUnwish:(id)sender
{
    if (![XUser current].token)
    {
        [[AppDelegate sharedAppDelegate] showLoginVC];
        return;
    }

    void (^answerBlock)(void) = ^(void) {
        [self hideHUD];
        [self updateUI];
    };

    void (^wishAnswerBlock)(BOOL success, NSError *error) = ^(BOOL success, NSError *error) {
        self.item.canBeWishedByMe = [NSNumber numberWithBool:!success];
        if (success)
        {
            int wishers = self.item.wishersCount.intValue;
            wishers++;
            self.item.wishersCount = [NSNumber numberWithInt:wishers];
        }
        answerBlock();
    };

    void (^unwishAnswerBlock)(BOOL success, NSError *error) = ^(BOOL success, NSError *error) {
        self.item.canBeWishedByMe = [NSNumber numberWithBool:success];
        if (success)
        {
            int wishers = self.item.wishersCount.intValue;
            wishers--;
            self.item.wishersCount = [NSNumber numberWithInt:wishers];
        }
        answerBlock();
    };

    if (self.item.canBeWishedByMe.boolValue)
    {
        [self showHUD:@"Wishing"];
        [[GIAClient sharedClient] wishGift:self.item withBlock:wishAnswerBlock];
    }
    else
    {
        [self showHUD:@"Unwishing"];
        [[GIAClient sharedClient] unwishGift:self.item withBlock:unwishAnswerBlock];
    }
}

//################################################################################
- (void) showHUD:(NSString *)msg
{
    NSLog(@"Showing Refresh HUD");
    UIView * view = self.view;
    
    self.refreshHUD = [[MBProgressHUD alloc] initWithView:view];
    self.refreshHUD.labelText = msg;
    self.refreshHUD.mode = MBProgressHUDModeIndeterminate;
    self.refreshHUD.dimBackground = NO;
    self.refreshHUD.minShowTime = 0.5;
    
    [view addSubview:self.refreshHUD];
    
    // Register for HUD callbacks so we can remove it from the window at the right time
    self.refreshHUD.delegate = self;
	
    // Show the HUD while the provided method executes in a new thread
    [self.refreshHUD show:YES];
}

//################################################################################
- (void) hideHUD
{
    if (self.refreshHUD)
    {
        [self.refreshHUD hide:YES];
    }
}

//################################################################################
#pragma mark - MBProgressHUDDelegate

//################################################################################
- (void)hudWasHidden:(MBProgressHUD *)hud
{
    // Remove HUD from screen when the HUD hides
    [self.refreshHUD removeFromSuperview];
	self.refreshHUD = nil;
}

//################################################################################
@end
//################################################################################
