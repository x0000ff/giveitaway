//
//  UserProfileVC.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 06.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "UserProfileVC.h"

#import "GIAItem.h"
#import "GIAItemPhoto.h"
#import "GIAUser.h"
#import "GIACategory.h"
#import "UIImageView+AFNetworking.h"

//################################################################################
@interface UserProfileVC ()

@property (nonatomic, strong) IBOutlet UIImageView * userAvatarIV;
@property (nonatomic, strong) IBOutlet UILabel * userNameLbl;

//@property (nonatomic, strong) IBOutlet UIImageView * itemIV;
//
//@property (nonatomic, strong) IBOutlet UILabel * titleLbl;
//@property (nonatomic, strong) IBOutlet UILabel * descriptionLbl;
//@property (nonatomic, strong) IBOutlet UILabel * categoryLbl;
//
//@property (nonatomic, strong) IBOutlet UILabel * dateLbl;

@end

//################################################################################
@implementation UserProfileVC

//################################################################################
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

//################################################################################
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUI];
}

//################################################################################
- (void) updateUI
{
    if (!self.isViewLoaded) return;
    
    self.userNameLbl.text = self.user.email;
    self.title = self.user.username;
    
    NSURL * avatarUrl = self.user.avatarUrl;
    [self.userAvatarIV setImageWithURL:avatarUrl placeholderImage:[UIImage imageNamed:@"fb_blank_profile_portrait.png"]];
}

//################################################################################
- (void) setUser:(GIAUser *)user
{
    _user = user;
    [self updateUI];
}

//################################################################################
@end
//################################################################################
