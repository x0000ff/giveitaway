//
//  PhotoVC.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 06.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "PhotoVC.h"
#import "UIImageView+AFNetworking.h"
#import "GIAItemPhoto.h"

//################################################################################
@interface PhotoVC ()

@property (nonatomic, strong) IBOutlet UIImageView * imageView;

- (IBAction)closeMe:(id)sender;

@end

//################################################################################
@implementation PhotoVC

//################################################################################
- (IBAction)closeMe:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//################################################################################
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Photo";
	// Do any additional setup after loading the view.
}

//################################################################################
- (void) setPhoto:(GIAItemPhoto *)photo
{
    _photo = photo;
    [self updateUI];
}

//################################################################################
- (void) updateUI
{
    if (!self.isViewLoaded) return;
    [self.imageView  setImageWithURL:self.photo.imageNormaUrl];
}

//################################################################################
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUI];
}

//################################################################################
@end
//################################################################################
