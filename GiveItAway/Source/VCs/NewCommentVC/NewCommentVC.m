//
//  NewCommentVC.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 06.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "NewCommentVC.h"
#import "UIImageView+AFNetworking.h"
#import "GIAComment.h"

//################################################################################
@interface NewCommentVC () <UITextViewDelegate, MBProgressHUDDelegate>

@property (nonatomic, strong) IBOutlet UITextView * contentTV;
@property (nonatomic, strong) IBOutlet UILabel * leftSymbolsLbl;
@property (nonatomic, strong) IBOutlet BButton * sendBtn;
@property (nonatomic, strong) MBProgressHUD *refreshHUD;

- (IBAction)closeMe:(id)sender;
- (IBAction)send:(id)sender;

@end

//################################################################################
static NSString * const kPlaceholderText = @"Enter your comment here...";
static NSUInteger const kMaximumSymbols = 144;

//################################################################################
@implementation NewCommentVC

//################################################################################
- (IBAction)closeMe:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//################################################################################
- (IBAction)send:(id)sender
{
    [self.contentTV resignFirstResponder];
    [self showHUD];

    NSString * contentText = self.contentTV.text;
    GIAItem * gift = self.item;
    [[GIAClient sharedClient] postCommentForGift:gift commentText:contentText withBlock:^(BOOL success, NSString *message, NSError *error) {
        NSLog(@"FINISH : %@", NSStringFromSelector(_cmd));
        
        [self hideHUD];
        if (!error && success)
        {
            
        }
        [self closeMe:nil];
    }];
}

//################################################################################
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"New Comment";

    self.contentTV.text = kPlaceholderText;
    self.contentTV.textColor = [UIColor grayColor];
    
    [self.sendBtn setType:BButtonTypeSuccess];
    self.sendBtn.titleLabel.text = @"Send";
}

//################################################################################
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.contentTV becomeFirstResponder];
}

//################################################################################
- (void) setItem:(GIAItem *)item
{
    _item = item;
    [self updateUI];
}

//################################################################################
- (void) updateUI
{
    if (!self.isViewLoaded) return;
    int symbols = ([self.contentTV.text isEqualToString:kPlaceholderText]) ? 0 : self.contentTV.text.length;
    self.leftSymbolsLbl.text = [NSString stringWithFormat:@"%d / %d", symbols, kMaximumSymbols];
    self.sendBtn.enabled = (self.leftSymbolsLbl.text.length > 0) && (self.leftSymbolsLbl.text.length <= kMaximumSymbols) && (![self.leftSymbolsLbl.text isEqualToString:kPlaceholderText]);
    self.sendBtn.userInteractionEnabled = self.sendBtn.enabled;
}

//################################################################################
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUI];
}

//################################################################################
- (void) showHUD
{
    [self hideHUD];
    NSLog(@"Post the comment...");
    UIView * view = self.navigationController.view;
    
    self.refreshHUD = [[MBProgressHUD alloc] initWithView:view];
    self.refreshHUD.labelText = @"Post the comment...";
    self.refreshHUD.dimBackground = YES;
    self.refreshHUD.minShowTime = 0.5;
    
    [view addSubview:self.refreshHUD];
	
    // Register for HUD callbacks so we can remove it from the window at the right time
    self.refreshHUD.delegate = self;
	
    // Show the HUD while the provided method executes in a new thread
    [self.refreshHUD show:YES];
}

//################################################################################
- (void) hideHUD
{
    if (self.refreshHUD)
    {
        [self.refreshHUD hide:YES];
    }
}


//################################################################################
#pragma mark - UITextViewDelegate

//################################################################################
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    NSLog(@"%@", NSStringFromSelector(_cmd));
    if ([textView.text isEqualToString:kPlaceholderText])
    {
        textView.text = nil;
        self.contentTV.textColor = [UIColor blackColor];
    }
}

//################################################################################
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSLog(@"%@", NSStringFromSelector(_cmd));
    if ([textView.text isEqualToString:kPlaceholderText])
    {
        textView.text = nil;
        self.contentTV.textColor = [UIColor blackColor];
    }

    return YES;
}

//################################################################################
- (void)textViewDidChange:(UITextView *)textView
{
    NSLog(@"%@", NSStringFromSelector(_cmd));
    if (!textView.text || textView.text.isEmpty)
    {
        self.contentTV.text = kPlaceholderText;
        self.contentTV.textColor = [UIColor grayColor];
    }
    [self updateUI];
}

//################################################################################
#pragma mark - MBProgressHUDDelegate

//################################################################################
- (void)hudWasHidden:(MBProgressHUD *)hud
{
    // Remove HUD from screen when the HUD hides
    [self.refreshHUD removeFromSuperview];
	self.refreshHUD = nil;
}

//################################################################################
@end
//################################################################################
