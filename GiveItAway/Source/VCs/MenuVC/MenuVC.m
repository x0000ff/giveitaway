//
//  MenuVC.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "MenuVC.h"
#import "IIViewDeckController.h"
#import "UIImageView+AFNetworking.h"
#import "ItemsVC.h"

//################################################################################
@interface MenuVC ()

@property (nonatomic, weak) IBOutlet UIImageView * userProfileIV;

@property (nonatomic, strong) IBOutlet UILabel * usernameLbl;
@property (nonatomic, strong) IBOutlet UILabel * someLbl;

@property (nonatomic, strong) IBOutlet UITableViewCell * loggedUserCell;
@property (nonatomic, strong) IBOutlet UITableViewCell * allGiftsCell;
@property (nonatomic, strong) IBOutlet UITableViewCell * myGiftsCell;
@property (nonatomic, strong) IBOutlet UITableViewCell * myWishesCell;

@property (nonatomic, strong) IBOutlet UITableViewCell * settingsCell;

@property (nonatomic, strong) IBOutlet UITableViewCell * loginLogoutCell;

@end

//################################################################################
@implementation MenuVC

//################################################################################
static NSUInteger const kLoggedUserSection = 0;
static NSUInteger const kLoggedUserSettingsSection = 1;
static NSUInteger const kLoginLogoutSection = 2;

static NSUInteger const kLoggedUserInfoCell = 0;
static NSUInteger const kAllGiftsCell = 1;
static NSUInteger const kMyGiftsCell = 1;
static NSUInteger const kMyWishesCell = 2;

static NSUInteger const kSettingsCell = 0;

static NSUInteger const kLoginCell = 0;
static NSUInteger const kLogoutCell = 0;

//################################################################################
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILabel * label = nil;
    
    label = self.myGiftsCell.textLabel;
    label.text = @"My Gifts";
    [label addAwesomeIcon:FAIconPicture beforeTitle:YES];
    
    label = self.myWishesCell.textLabel;
    label.text = @"My Wishes";
//    [label addAwesomeIcon:FAIconEyeOpen beforeTitle:YES];
//    [label addAwesomeIcon:FAIconFlag beforeTitle:YES];
//    [label addAwesomeIcon:FAIconFlagAlt beforeTitle:YES];
    [label addAwesomeIcon:FAIconBookmark beforeTitle:YES];
    
    label = self.allGiftsCell.textLabel;
    label.text = @"Gifts";
    [label addAwesomeIcon:FAIconSearch beforeTitle:YES];
    
    label = self.settingsCell.textLabel;
    label.text = @"Settings";
    [label addAwesomeIcon:FAIconCog beforeTitle:YES];

    if ([XUser current].token && ![XUser current].user)
    {
        [GIAUser myProfileWithBlock:^(GIAUser *user, NSError *error) {
            [XUser current].user = user;
            [[XUser current] save];
            [self updateUI];
        }];
    }
    
//    [XUser current];
    [XUser loadCachedSession];
}

//################################################################################
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updateUI];
}

//################################################################################
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:kFacebookStateDidChange object:nil];
}

//################################################################################
- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}

//################################################################################
- (void) updateUI
{
    if (!self.isViewLoaded) return;
    
    BOOL isLogged = ([XUser current].token != nil);
    
    self.loginLogoutCell.textLabel.text = isLogged ? @"Logout" : @"Login";
    [self.loginLogoutCell.textLabel addAwesomeIcon:(isLogged ? FAIconSignout : FAIconSignin) beforeTitle:YES];

    self.usernameLbl.text = [XUser current].user.first_name;
    self.someLbl.text = [XUser current].user.email;
    
    [self.userProfileIV setImageWithURL:[[XUser current] avatarUrl] placeholderImage:[XUser avatarPlaceholderImage]];
}


//################################################################################
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@">>> %d :: %d", indexPath.section, indexPath.row);
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (cell == self.loginLogoutCell)
    {
        if ([XUser isLogged])
        {
            [XUser logout];
            [self updateUI];
        }
        else
        {
            [[AppDelegate sharedAppDelegate] showLoginVC];
        }
        return;
    }
    else if (cell == self.allGiftsCell)
    {
        [self showItemsAsCenterVcWithType:ItemInfoVcTypeAll];
    }
    else if (cell == self.myGiftsCell)
    {
        [self showItemsAsCenterVcWithType:ItemInfoVcTypeMyGifts];
    }
    else if (cell == self.myWishesCell)
    {
        [self showItemsAsCenterVcWithType:ItemInfoVcTypeMyWishes];
    }
    
    [self.viewDeckController closeLeftView];
}

//################################################################################
- (void) showItemsAsCenterVcWithType:(ItemInfoVcType)itemsType
{
    UINavigationController * centerNVC = (UINavigationController *)self.viewDeckController.centerController;
    ItemsVC * itemsVC = nil;
    
    if ([centerNVC isKindOfClass:UINavigationController.class])
    {
        itemsVC = (ItemsVC *)centerNVC.viewControllers[0];
        if ([itemsVC isKindOfClass:ItemsVC.class])
        {
            [centerNVC popToRootViewControllerAnimated:YES];
        }
        else
        {
            itemsVC = nil;
        }
    }

    if (!itemsVC)
    {
        centerNVC = (UINavigationController *)[[AppDelegate storyboard] instantiateViewControllerWithIdentifier: @"ItemsNVC"];
        itemsVC = (ItemsVC *)centerNVC.viewControllers[0];
    }
    
    if ([itemsVC isKindOfClass:ItemsVC.class])
    {
        itemsVC.itemsType = itemsType;
    }
    self.viewDeckController.centerController = centerNVC;
}

//################################################################################
#pragma mark - UITableViewDataSource

//################################################################################
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    return nil;
//}

//################################################################################
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @" ";
}

//################################################################################
@end
//################################################################################
