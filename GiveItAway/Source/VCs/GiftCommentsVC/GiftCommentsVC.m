//
//  GiftCommentsVC.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "GiftCommentsVC.h"

#import "UserProfileVC.h"
#import "CommentCell.h"
#import "NewCommentVC.h"

#import "GIAUser.h"
#import "GIAItem.h"
#import "GIAComment.h"

//################################################################################
@interface GiftCommentsVC () <UITableViewDelegate, UITableViewDataSource, MBProgressHUDDelegate>

@property (nonatomic, strong) NSMutableArray * objects;
@property (nonatomic, strong) IBOutlet UITableView * tableView;
@property (nonatomic, strong) IBOutlet UIView * tableViewHeader;
@property (nonatomic, strong) MBProgressHUD *refreshHUD;
@property (nonatomic, assign) BOOL loading;

- (IBAction)reload:(__unused id)sender;

@end

//################################################################################
@implementation GiftCommentsVC

//################################################################################
- (void) viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Comments";
    self.tableView.tableHeaderView = nil;
}

//################################################################################
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.tableView.tableHeaderView = ([XUser current].token) ? self.tableViewHeader : nil;
    [self addRefreshButton];
    if ([self.objects count] <= 0)
    {
        [self reload:nil];
    }
}

//################################################################################
- (void) setItem:(GIAItem *)item
{
    _item = item;
    [self reload:nil];
}

//################################################################################
- (void) addRefreshButton
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reload:)];
}

//################################################################################
- (void) addActivityButton
{
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicatorView];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicatorView];
}

//################################################################################
- (IBAction)reload:(__unused id)sender
{
    if (self.loading) return;
    
    self.loading = YES;
    
    [self showHUD];
    [self addActivityButton];
    self.objects = nil;

    [[GIAClient sharedClient] giftComments:self.item withBlock:^(NSArray *comments, NSError *error) {
        NSLog(@"FINISH : %@", NSStringFromSelector(_cmd));
        
        self.loading = NO;
        if (!error)
        {
            if (!self.objects)
            {
                self.objects = [NSMutableArray array];
            }
            
            if (comments)
            {
                [self.objects addObjectsFromArray:comments];
            }
        }
        
        [self.tableView reloadData];
        [self hideHUD];
        [self addRefreshButton];
    }];
}

//################################################################################
- (void) showHUD
{
    [self hideHUD];
    NSLog(@"Showing Refresh HUD");
    UIView * view = self.view;
    
    self.refreshHUD = [[MBProgressHUD alloc] initWithView:view];
    self.refreshHUD.labelText = @"Loading";
    self.refreshHUD.dimBackground = NO;
    self.refreshHUD.minShowTime = 0.5;
    
    [view addSubview:self.refreshHUD];

    // Register for HUD callbacks so we can remove it from the window at the right time
    self.refreshHUD.delegate = self;
	
    // Show the HUD while the provided method executes in a new thread
    [self.refreshHUD show:YES];
}

//################################################################################
- (void) hideHUD
{
    if (self.refreshHUD)
    {
        [self.refreshHUD hide:YES];
    }
}

//################################################################################
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showUserProfile"])
    {
        GIAComment * comment = [self.objects objectAtIndex:self.tableView.indexPathForSelectedRow.row];
        UserProfileVC * vc = nil;
        
        if ([segue.destinationViewController isKindOfClass:UINavigationController.class])
        {
            vc = (UserProfileVC *) [segue.destinationViewController viewControllers][0];
        }
        else
        {
            vc = (UserProfileVC *) segue.destinationViewController;
        }
        
        vc.user = comment.user;
    }
    else if ([segue.identifier isEqualToString:@"newComment"])
    {
        NewCommentVC * vc = nil;
        
        if ([segue.destinationViewController isKindOfClass:UINavigationController.class])
        {
            vc = (NewCommentVC *) [segue.destinationViewController viewControllers][0];
        }
        else
        {
            vc = (NewCommentVC *) segue.destinationViewController;
        }
        
        vc.item = self.item;
        self.objects = nil;
    }
    
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

//################################################################################
#pragma mark - UITableViewDataSource

//################################################################################
- (NSInteger)tableView:(__unused UITableView *)tableView numberOfRowsInSection:(__unused NSInteger)section
{
    return [self.objects count];
}

//################################################################################
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:[CommentCell cellIdentifier]];
    cell.comment = [self.objects objectAtIndex:indexPath.row];

    return cell;
}

//################################################################################
#pragma mark - UITableViewDelegate

//################################################################################
- (CGFloat)tableView:(__unused UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CommentCell cellHeight];
}

//################################################################################
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GIAComment * comment = [self.objects objectAtIndex:self.tableView.indexPathForSelectedRow.row];
    if (comment.isMine)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    [self performSegueWithIdentifier: @"showUserProfile" sender: self];
}


//################################################################################
#pragma mark - MBProgressHUDDelegate

//################################################################################
- (void)hudWasHidden:(MBProgressHUD *)hud
{
    // Remove HUD from screen when the HUD hides
    [self.refreshHUD removeFromSuperview];
	self.refreshHUD = nil;
}

//####################################################################
@end
//################################################################################
