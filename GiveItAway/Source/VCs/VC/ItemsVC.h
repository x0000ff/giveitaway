//
//  ItemsVC.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <UIKit/UIKit.h>
#import "CenterVC.h"

//################################################################################
enum {
    ItemInfoVcTypeAll = 0,
    ItemInfoVcTypeMyGifts = 1,
    ItemInfoVcTypeMyWishes = 2,
};
typedef UInt32 ItemInfoVcType;

//################################################################################
@interface ItemsVC : CenterVC

//################################################################################
@property (nonatomic, assign) ItemInfoVcType itemsType;

//################################################################################
@end
//################################################################################
