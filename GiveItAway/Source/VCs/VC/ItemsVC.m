//
//  ItemsVC.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "ItemsVC.h"

#import "GIAUser.h"
#import "ItemCell.h"

#import "UIActivityIndicatorView+AFNetworking.h"
#import "UIAlertView+AFNetworking.h"
#import "ItemInfoVC.h"

//################################################################################
@interface ItemsVC () <UITableViewDelegate, UITableViewDataSource, MBProgressHUDDelegate,UISearchBarDelegate, UISearchDisplayDelegate, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) NSMutableArray * objects;
@property (nonatomic, strong) IBOutlet UITableView * tableView;
@property (nonatomic, strong) MBProgressHUD *refreshHUD;
@property (nonatomic, assign) BOOL loading;
@property (nonatomic, strong) NSString * requestUrl;

@property (nonatomic, strong) NSString * searchTerm;

- (IBAction)reload:(__unused id)sender;

@end

//################################################################################
@implementation ItemsVC

//################################################################################
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // Set the return key and keyboard appearance of the search bar
    for (UIView *searchBarSubview in [self.searchBar subviews])
    {
        [self configureTextField:searchBarSubview];
        for (UIView *searchBarSubSubview in searchBarSubview.subviews)
        {
            [self configureTextField:searchBarSubSubview];
        }
    }
}

//################################################################################
- (void) configureTextField:(NSObject *)object
{
    if (![object conformsToProtocol:@protocol(UITextInputTraits)])
    {
        return;
    }
    
    UITextField * tf = (UITextField *)object;
    @try
    {
        [tf setReturnKeyType:UIReturnKeySearch];
        [tf setEnablesReturnKeyAutomatically:NO];
        [tf setClearButtonMode:UITextFieldViewModeAlways];
//        [tf setKeyboardAppearance:UIKeyboardAppearanceAlert];
    }
    @catch (NSException * e)
    {
        // ignore exception
    }
}

//################################################################################
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self addRefreshButton];
    
    if ([self.objects count] <= 0)
    {
        [self reload:nil];
    }
}

//################################################################################
- (void) setItemsType:(ItemInfoVcType)itemsType
{
    if (itemsType == _itemsType) return;
    
    _itemsType = itemsType;
    self.searchBar.text = nil;
    [self reload:nil];
}

//################################################################################
- (void) addRefreshButton
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reload:)];
}

//################################################################################
- (void) addActivityButton
{
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicatorView];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicatorView];
}

//################################################################################
- (IBAction)reload:(__unused id)sender
{
    if (self.loading) return;
    
    self.loading = YES;
    
    [self showHUD];
    [self addActivityButton];
    self.objects = nil;
    
    void (^answerBlock)(NSArray *gifts, NSError *error, NSString * requestUrl) = ^(NSArray *gifts, NSError *error, NSString * requestUrl) {
        NSLog(@"FINISH : %@ :: %@", NSStringFromSelector(_cmd), requestUrl);
        
        if (![self.requestUrl isEqualToString:requestUrl])
        {
            return;
        }
        self.loading = NO;
        if (!error)
        {
            if (!self.objects)
            {
                self.objects = [NSMutableArray array];
            }
            
            if (gifts)
            {
                [self.objects addObjectsFromArray:gifts];
            }
        }
        
        [self.tableView reloadData];
        [self hideHUD];
        [self addRefreshButton];
    };

    switch (self.itemsType) {
        case ItemInfoVcTypeAll:
            self.title = @"All Gifts";
            self.requestUrl = [[GIAClient sharedClient] allGiftsWithFilterString:self.searchTerm block:answerBlock];
            break;
        case ItemInfoVcTypeMyGifts:
            self.title = @"My Gifts";
            self.requestUrl = [[GIAClient sharedClient] myGiftsWithFilterString:self.searchTerm block:answerBlock];
            break;
        case ItemInfoVcTypeMyWishes:
            self.title = @"Gifts I wish";
            self.requestUrl = [[GIAClient sharedClient] myWishesWithFilterString:self.searchTerm block:answerBlock];
            break;
    }
}

//################################################################################
- (void) showHUD
{
    [self hideHUD];
    NSLog(@"Showing Refresh HUD");
    UIView * view = self.view;
    
    self.refreshHUD = [[MBProgressHUD alloc] initWithView:view];
    self.refreshHUD.labelText = @"Loading";
    self.refreshHUD.mode = MBProgressHUDModeIndeterminate;
    self.refreshHUD.dimBackground = NO;
    self.refreshHUD.minShowTime = 0.5;
    
    [view addSubview:self.refreshHUD];
    
    // Register for HUD callbacks so we can remove it from the window at the right time
    self.refreshHUD.delegate = self;
	
    // Show the HUD while the provided method executes in a new thread
    [self.refreshHUD show:YES];

}

//################################################################################
- (void) hideHUD
{
    if (self.refreshHUD)
    {
        [self.refreshHUD hide:YES];
    }
}

//################################################################################
#pragma mark - MBProgressHUDDelegate

//################################################################################
- (void)hudWasHidden:(MBProgressHUD *)hud
{
    // Remove HUD from screen when the HUD hides
    [self.refreshHUD removeFromSuperview];
	self.refreshHUD = nil;
}

//################################################################################
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showItem"])
    {
//        NSLog(@">>> %@", NSStringFromClass([segue.destinationViewController class]));
        ItemInfoVC * vc = (ItemInfoVC *)segue.destinationViewController;
        vc.item = (GIAItem *) [self.objects objectAtIndex:self.tableView.indexPathForSelectedRow.row];
    }
    
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

//################################################################################
#pragma mark - UITableViewDataSource

//################################################################################
- (NSInteger)tableView:(__unused UITableView *)tableView numberOfRowsInSection:(__unused NSInteger)section
{
    return [self.objects count];
}

//################################################################################
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ItemCell *cell = [tableView dequeueReusableCellWithIdentifier:[ItemCell cellIdentifier]];
    cell.item = [self.objects objectAtIndex:indexPath.row];
    
    return cell;
}

//################################################################################
#pragma mark - UITableViewDelegate

//################################################################################
- (CGFloat)tableView:(__unused UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [ItemCell cellHeight];
}

//################################################################################
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//####################################################################
#pragma mark Content Filtering

//####################################################################
-(void)filterContentForSearchText:(NSString*)searchText
{
    if (![self.searchTerm isEqualToString:searchText])
    {
        self.searchTerm = searchText;
        [self reload:nil];
    }
}

//####################################################################
#pragma mark - UISearchBarDelegate

//################################################################################
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

//################################################################################
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self filterContentForSearchText:searchBar.text];
}

//################################################################################
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{

}

//################################################################################
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}

//####################################################################
@end
//################################################################################
