//
//  LoginVC.m
//  xKarmaApp
//
//  Created by Konstantin Portnov on 03.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "LoginVC.h"
#import "AppDelegate.h"

//################################################################################
@interface LoginVC ()

@property (nonatomic, weak) IBOutlet UILabel * progressLbl;
@property (nonatomic, weak) IBOutlet BButton * loginBtn;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView * indicator;

//################################################################################
- (IBAction)doLogin:(id)sender;
- (IBAction)doCancel:(id)sender;

@end

//################################################################################
@implementation LoginVC

//################################################################################
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.progressLbl.text = @"";
    self.indicator.hidesWhenStopped = YES;
    
    // Button
    [self.loginBtn addAwesomeIcon:FAIconFacebookSign beforeTitle:YES];
    [self.loginBtn setType:BButtonTypeFacebook];
}

//################################################################################
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:kFacebookStateDidChange object:nil];
    
    [self updateUI];
}

//################################################################################
- (void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

//################################################################################
- (void) updateUI
{
    if (!self.isViewLoaded) return;
    
    BOOL isLogging = [XUser current].isLogging;
    
    self.progressLbl.text = isLogging ? @"Logging in..." : nil;
    self.progressLbl.hidden = !isLogging;
    self.loginBtn.userInteractionEnabled = !isLogging;

    if(isLogging) [self.indicator startAnimating];
    else [self.indicator stopAnimating];
    
    if ([XUser isLogged])
    {
        [self performSelector:@selector(doCancel:) withObject:nil afterDelay:1];
    }
}

//################################################################################
- (IBAction)doLogin:(id)sender
{
    [XUser login];
}

//################################################################################
- (IBAction)doCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//################################################################################
@end
//################################################################################
