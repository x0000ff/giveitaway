//
//  AppDelegate.h
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <UIKit/UIKit.h>

#import <FacebookSDK/FacebookSDK.h>

//################################################################################
@interface AppDelegate : UIResponder <UIApplicationDelegate>

//################################################################################
@property (strong, nonatomic) UIWindow *window;

//################################################################################
+ (AppDelegate *) sharedAppDelegate;
+ (UIStoryboard *) storyboard;

- (void) showLoginVC;

//################################################################################
@end
//################################################################################
