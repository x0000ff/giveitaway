//
//  AppDelegate.m
//  GiveItAway
//
//  Created by Konstantin Portnov on 05.10.13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "AppDelegate.h"
#import "IIViewDeckController.h"

#import "MenuVC.h"
#import <FacebookSDK/FacebookSDK.h>
//#import "DBFBProfilePictureView.h"

#import "LoginVC.h"

//################################################################################
@implementation AppDelegate

//################################################################################
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Otherwise IB doesn't load "FBProfilePictureView"
    [FBProfilePictureView class];
//    [DBFBProfilePictureView class];
//    [XUser current];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UINavigationController * centerNVC = (UINavigationController *)[[self.class storyboard] instantiateViewControllerWithIdentifier: @"ItemsNVC"];

    MenuVC * menuVC = (MenuVC *)[[self.class storyboard] instantiateViewControllerWithIdentifier: @"Menu"];
    IIViewDeckController * rootDeckController =  [[IIViewDeckController alloc] initWithCenterViewController:centerNVC
                                                                                          leftViewController:menuVC];
    rootDeckController.leftSize = 44;
    rootDeckController.delegateMode = IIViewDeckDelegateAndSubControllers;

    self.window.rootViewController = rootDeckController;
    [self.window makeKeyAndVisible];

    return YES;
}

//################################################################################
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

//################################################################################
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

//################################################################################
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

//################################################################################
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

//################################################################################
- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//################################################################################
#pragma  mark - My stuff

//################################################################################
+ (UIStoryboard *) storyboard
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    return mainStoryboard;
}

//################################################################################
+ (AppDelegate *) sharedAppDelegate
{
    AppDelegate * appDelegate = [UIApplication sharedApplication].delegate;
    return appDelegate;
}

//################################################################################
- (void) showLoginVC
{
    LoginVC * vc = [[self.class storyboard] instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.window.rootViewController presentViewController:vc animated:YES completion:nil];
}

//################################################################################
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [XUser handleOpenURL:url];
}

//################################################################################
@end
//################################################################################
